/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 * ©2016, Pavel Panchekha and the University of Washington.
 * Released under the MIT license.
 *
 * Rewritten in TypeScript and extended
 * ⓒ2017, Alex Polozov and the University of Washington.
 */

//// The AST

// This class represents all AST Nodes
class ASTNode {
    type: string;

    joinApplyPredicate(x) {
        if (x.right instanceof Array) {
            return x.right;
        } else {
            Object.assign(x.left, x.right);
            return x.left;
        }
    }

    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
        throw new Error("Unimplemented AST node " + this.type);
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 Implement call-chaining
    filter(predicate: (a: any) => boolean) {
        return new ThenNode(this, new FilterNode(predicate));
    }

    apply(predicate: (a : any) => any) {
        return new ThenNode(this, new ApplyNode(predicate));
    }

    count() {
        return new ThenNode(this, new CountNode());
    }

    product(right: ASTNode) {
        return new CartesianProductNode(this, right);
    }

    join(right: ASTNode, predicate: ((l: any, r:any) => boolean) | string) {
        let prod = this.product(right);
        var newPred;
        if (typeof predicate === "string") {
            newPred = MakeStringPredicate(predicate);
        } else {
            newPred = ((x) => predicate(x.left, x.right));
        }
        let filtered = prod.filter(newPred);
        return filtered.apply(this.joinApplyPredicate);
    }
}

// The Id node just outputs all records from input.
class IdNode extends ASTNode {

    constructor() {
        super("Id");
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return data;
    }
}

// The Filter node uses a callback to throw out some records
class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        var result: any[] = [];
        for (let a of data) {
            if (this.predicate(a)) {
                result.push(a);
            }
        }
        return result;
    }

}

// The Then node chains multiple actions on one data structure
class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return this.second.execute(this.first.execute(data));
    }

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }
}


//// 1.2 Write a Query
// Define the `theftsQuery` and `autoTheftsQuery` variables

let theftsQuery = new FilterNode(x => x[13].indexOf('THEFT') >= 0);
let autoTheftsQuery = new FilterNode(x => x[13].indexOf('VEH-THEFT') >= 0);

//// 1.3 Add Apply and Count Nodes
class ApplyNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (any) => any) {
        super("Apply");
        this.predicate = predicate;
    }

    execute(data: any[]): any {
        var result: any[] = [];

        for (let a of data) {
            result.push(this.predicate(a));
        }
        return result;
    }
}

class CountNode extends ASTNode {
    constructor() {
        super("Count");
    }

    execute(data: any[]): any[] {
        return [data.length];
    }
}

//// 1.4 Clean the data

let cleanupQuery = new ApplyNode(x => ({type: x[13], description: x[15], date: x[17], area: x[19]}));

let Q = new IdNode();

//// 1.6 Reimplement queries with call-chaining

let cleanupQuery2 = Q.apply(x => ({type: x[13], description: x[15], date: x[17], area: x[19]})); // ...

let theftsQuery2 = Q.filter(x => x.type.indexOf('THEFT') >= 0); // ...

let autoTheftsQuery2 = Q.filter(x => x.type.indexOf('VEH-THEFT') >= 0); // ...

//// 2.1 Optimize Queries

function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    let old = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function (this: ASTNode): ASTNode {
        let newThis = old.call(this);
        return opt.call(newThis) || newThis;
    }
}

function CombineFilters(this: any) : ASTNode {
    if (this.first instanceof ThenNode && this.first.second instanceof FilterNode && this.second instanceof FilterNode) {
        this.first.second = new FilterNode(x => this.first.second.predicate(x) && this.second.predicate(x));
        return this.first;
    }
}

function MakeCountIf(this: any) : ASTNode {
    if (this.first instanceof ThenNode && this.first.second instanceof FilterNode && this.second instanceof CountNode) {
        this.first.second = new CountIf(this.first.second.predicate);
        return this.first;
    }
}

AddOptimization(ThenNode, CombineFilters);
AddOptimization(ThenNode, MakeCountIf);

//// 2.2 Internal node types and CountIf
class CountIf extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (any) => any) {
        super("CountIf");
        this.predicate = predicate;
    }

    execute(data: any[]): number {
        var count: number;

        for (let a of data) {
            if (this.predicate(a)) count++;
        }

        return count;
    }
}

//// 3.1 Cartesian Products
class CartesianProductNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;

    constructor(left: ASTNode, right: ASTNode) {
        super("CartesianProduct");
        this.left = left;
        this.right = right;
    }

    execute(data: any[]): any {
        let leftRecord = this.left.execute(data); 
        let rightRecord = this.right.execute(data);

        let product: any[] = [];

        for (let left of leftRecord) {
            for (let right of rightRecord) {
                product.push({left, right});
            }
        }

        return product;
    }

    optimize(): ASTNode {
        return new CartesianProductNode(this.left.optimize(), this.right.optimize());
    }
}

//// 3.2 Joins

//// 3.3 Optimizing joins

function MakeStringPredicate(name: string) : NamedFunction {
    let newPred:NamedFunction = <NamedFunction>function (x) {
        return x.left[name] == x.right[name]}
    newPred.fname = name;
    return newPred;
}

class JoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    predicate: ((x) => boolean);

    constructor(predicate: ((x) => boolean) | string, left: ASTNode, right: ASTNode) {
        super("Join");
        this.left = left;
        this.right = right;
        if (typeof predicate === "string") {
            this.predicate = MakeStringPredicate(predicate);
        } else {
            this.predicate = predicate;
        }
    }

    execute(data: any[]): any {
        let leftRecord = this.left.execute(data);
        let rightRecord = this.right.execute(data);
        
        let join: any[] = [];

        for (let left of leftRecord) {
            for (let right of rightRecord) {
                if ((this.predicate as (x) => boolean)({left:left, right:right})) {
					join.push(this.joinApplyPredicate({left:left, right:right}));
                }
            }
        }
        return join;
    }

    optimize(): ASTNode {
        return new JoinNode(this.predicate, this.left.optimize(), this.right.optimize());
    }
}

function OptimizeJoin(this: any) : ASTNode {
    if (this.second instanceof ApplyNode && this.first instanceof ThenNode && 
            this.first.second instanceof FilterNode && this.first.first instanceof CartesianProductNode) {
        if (this.second.predicate == Q.joinApplyPredicate) {
            return new JoinNode(this.first.second.predicate, this.first.first.left,
                this.first.first.right);
        }
    }
    return this;
}


//// 3.4 Join on fields

//// 3.5 Implement hash joins


function makeMap(data: any[], field: string) {
    var map = {}
    for (let row of data) {
		if (map.hasOwnProperty(row[field])) {
			map[row[field]].push(row);
		} else {
			map[row[field]] = [row];
		}
    }
    return map;
}

class HashJoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    predicate: string;

    constructor(predicate: string, left: ASTNode, right: ASTNode) {
        super("HashJoin");
        this.left = left;
        this.right = right;
        this.predicate = predicate;
    }

    execute(data: any[]): any {
        let leftRecord = this.left.execute(data);
        let rightRecord = this.right.execute(data);

        let join: any[] = [];

        let leftMap = makeMap(leftRecord, this.predicate);
        let rightMap = makeMap(rightRecord, this.predicate);

        for (var key in leftMap) {
            if (rightMap.hasOwnProperty(key)) {
				for (var leftRow in leftMap[key]) {
					for (var rightRow in rightMap[key]) {
						var rowCopy = Object.assign({}, leftRow);
						Object.assign(rowCopy, rightRow);
						join.push(rowCopy);
					}
				}
            }
        }
		return join
    }
}

AddOptimization(ThenNode, OptimizeHashJoin);
AddOptimization(ThenNode, OptimizeJoin);

function OptimizeHashJoin(this: any) : ASTNode {
    if (this.second instanceof ApplyNode && this.first instanceof ThenNode && 
            this.first.second instanceof FilterNode && this.first.first instanceof CartesianProductNode) {
        if (this.second.predicate == Q.joinApplyPredicate && isNamedFunction(this.first.second.predicate)) {
            return new HashJoinNode(this.first.second.predicate.fname, this.first.first.left,
                this.first.first.right);
        }
    }
    return this;
}

function OptimizeJoinToHashJoin(this: any) : ASTNode {
    if (isNamedFunction(this.predicate)) {
        return new HashJoinNode(this.predicate.fname, this.left, this.right);
    }
    return this;
}

AddOptimization(JoinNode, OptimizeJoinToHashJoin);

//// 3.6 Optimize joins on fields to hash joins
interface NamedFunction {
    (x: any): any;
    fname: string;
}

function isNamedFunction(f: any): f is NamedFunction {
    return ("fname" in f) && (typeof f.fname === "string");
}

export class EngExpError extends Error {
    constructor(...params) {
        super(...params);
    }
}

enum GroupType {
    BaseExp,
    Cap,
    NonCap
}

export class EngExp {
    private flags: string = "m";
    private pattern: string = "";
    // you can add new fields here!
    private parentExp: EngExp = undefined;
    private nestGroup: GroupType = GroupType.BaseExp;

    // don't change sanitize()
    private static sanitize(s: string | EngExp): string | EngExp {
        if (s instanceof EngExp)
            return s;
        else {
            return s.replace(/[^A-Za-z0-9_]/g, "\\$&");
        }
    }

    // don't change these either
    public toString(): string {
        return this.asRegExp().source;
    }
    public valueOf(): string {
        return this.asRegExp().source;
    }
    public withFlags(flags: string) {
        if (/[^gimuy]/g.test(flags))
            throw new EngExpError("invalid flags");
        this.flags = "".concat(...new Set(flags));
        return this;
    }

    // asRegExp() will always be called before using your EngExp as a RegExp,
    // or before converting it into a string (for example to include via ``
    // template literals in another EngExp). You might want to take advantage
    // of this to check for errors...
    public asRegExp(): RegExp {
        if (this.nestGroup != GroupType.BaseExp) {
            throw EngExpError;
        }
        return new RegExp(this.pattern, this.flags);
    }

    // The silly TAs left a bug somewhere in this code... can you find it?

    public match(literal: string): EngExp {
        return this.then(literal);
    }

    public then(pattern: string | EngExp): EngExp {
        this.pattern += `(?:${EngExp.sanitize(pattern)})`;
        return this;
    }

    public startOfLine(): EngExp {
        this.pattern += "^";
        return this;
    }

    public endOfLine(): EngExp {
        this.pattern += "$";
        return this;
    }

    public zeroOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.zeroOrMore());
        else {
            this.pattern = `(?:${this.pattern})*`;
            return this;
        }
    }

    public oneOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.oneOrMore());
        else {
            this.pattern = `(?:${this.pattern})+`;
            return this;
        }
    }

    public optional(): EngExp {
        this.pattern = `(?:${this.pattern})?`;
        return this;
    }

    public maybe(pattern: string | EngExp): EngExp {
        this.pattern += `(?:${EngExp.sanitize(pattern)})?`;
        return this;
    }

    public anythingBut(characters: string): EngExp {
        this.pattern += `[^${EngExp.sanitize(characters)}]*`;
        return this;
    }

    public digit(): EngExp {
        this.pattern += "\\d";
        return this;
    }

    public repeated(from: number, to?: number): EngExp {
        this.pattern = `(?:${this.pattern}){${from},${to}}`;
        return this;
    }

    public multiple(pattern: string | EngExp, from: number, to?: number) {
        this.pattern += `(?:${EngExp.sanitize(pattern)}){${from},${to}}`;
        return this;
    }

    // you need to implement these five operators:

    public or(pattern: string | EngExp): EngExp {
        this.pattern += `|(?:${EngExp.sanitize(pattern)})`;
        return this;
    }

    public beginLevel(): EngExp {
        var childExp = new EngExp();
        childExp.nestGroup = GroupType.NonCap
        childExp.parentExp = this;
        return childExp;
    }

    public endLevel(): EngExp {
        if (this.nestGroup != GroupType.NonCap) {
            throw EngExpError;
        }
        this.nestGroup = GroupType.BaseExp
        return this.parentExp.then(this);
    }

    public beginCapture(): EngExp {
        var childExp = new EngExp();
        childExp.nestGroup = GroupType.Cap;
        childExp.parentExp = this;
        return childExp;
    }

    public endCapture(): EngExp {
        if (this.nestGroup != GroupType.Cap) {
            throw EngExpError;
        }
        this.nestGroup = GroupType.BaseExp
        this.parentExp.pattern += `(${this.pattern})`;
        return this.parentExp
    }
}

// ---- EXTRA CREDIT: named capture groups ----

// You'll need to modify a few parts of your EngExp implementation.
// First, change the signatures of beginCapture() and endCapture() to the following:
//
//     beginCapture(name?: string): EngExp
//     endCapture(name?: string): EngExp
//
// JavaScript regular expressions don't support named groups out of the box,
// so we'll need to extend them. Change the signature of asRegExp() to the following:
//
//     asRegExp(): NamedRegExp
//
// The interface for NamedRegExp is provided below (don't change it!) and depends on
// the new interface for NamedRegExpExecArray.

interface NamedRegExpExecArray extends RegExpExecArray {
    groups: Map<string, string | undefined>;
}
interface NamedRegExp extends RegExp {
    exec(s: string): NamedRegExpExecArray | null;
}

// Essentailly, a NamedRegExp is like a RegExp, but when you call its exec() method
// you get back an array with an extra field, groups, which is a hashmap from the
// names of capture groups to what those groups matched. You can access it like this:
//
//     let r = new EngExp().beginCapture("justf").then("f").endCapture().then("oo").asRegExp()
//     r.exec("foo").groups["justf"]  ==  "f"
//
// See the tests at the end of test/engexp.ts for more exampls of usage.

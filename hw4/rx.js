
function Stream() {
    this.callbacks = []
}

// PART 1 HERE
Stream.prototype.subscribe = function(callback) {
    this.callbacks.push(callback);
}

Stream.prototype._push = function(arg) {
    // Call each of our callbacks with given arguements
    for (var i = 0; i < this.callbacks.length; i++) {
        // apply needs arguements in an array
        this.callbacks[i].apply(this, [arg]);
    }
}

Stream.prototype._push_many = function(args) {
    // Need to save reference so we can use in our anonymous function
    for (var i = 0; i < args.length; i++) {
        this._push(args[i]);
    }
}

Stream.prototype.first = function() {
    var empty = true;
    var stream = new Stream();
    this.subscribe(function(x) {
        if (empty) {
            empty = false;
            stream._push(x);
        }
    });
    return stream;
}

Stream.prototype.map = function(f) {
    var stream = new Stream();
    this.subscribe(function(x) {
        stream._push(f(x));
    });
    return stream;
}

Stream.prototype.filter = function(f) {
    var stream = new Stream();
    this.subscribe(function(x) {
        if (f(x)) {
            stream._push(x);
        }
    });
    return stream;
}

Stream.prototype.flatten = function() {
    var stream = new Stream();
    this.subscribe(function(x) {
        stream._push_many(x);
    });
    return stream;
}

Stream.prototype.join = function(B) {
    var stream = new Stream();
    this.subscribe(function(x) {stream._push(x)});
    B.subscribe(function(x) {stream._push(x)});
    return stream;
}

Stream.prototype.combine = function() {
    var stream = new Stream();
    this.subscribe(function(s) {
        s.subscribe(function(x) {
            stream._push(x);
        });
    });
    return stream;
}

Stream.prototype.zip = function(B, f) {
    var stream = new Stream();
    var lastA;
    var lastB;
    this.subscribe(function(x) {
        lastA = x;
        if (lastB) {
            stream._push(f(lastA, lastB));
        }
    });
    B.subscribe(function(x) {
        lastB = x;
        if (lastA) {
            stream._push(f(lastA, lastB));
        }
    });
    return stream;
}

Stream.timer = function(N) {
    var stream = new Stream();
    setInterval(function() {
        stream._push(new Date());
    }, N);
    return stream;
}

Stream.dom = function(element, eventname) {
    var stream = new Stream();
    element.on(eventname, function(e) {
        stream._push(e);
    });
    return stream;
}

Stream.prototype.throttle = function(N) {
    var stream = new Stream();
    var pending = false;
    var lastValue;

    this.subscribe(function(x) {
        lastValue = x;
        if (!pending) {
            setTimeout(function() {
                stream._push(lastValue);
                pending = false;
            }, N);
            pending = true;
        }
    });
    return stream;
}

Stream.url = function(url) {
    var stream = new Stream();
    $.get(url, function(json) {
        stream._push(json);
    }, "json");
    return stream;
}

/*
This function is already optimized. Note that whenever we get a new 
stream that is producing new values, we simply prune off the old streams
so we don't have to worry about keeping an index element around. This provides 
us with a minimal optimization of not having to keep an extra index element 
around, but it also allows our stream list to not grow out of control, 
particularly if there are a significant number of streams that a produced from
our base stream. One other optimization that we could do would be to remove the 
old streams from our subscribers list. Whether this would result in a speed 
increase would depend on how many additional events we would expect to receive 
on average once a new stream has started producing events.
*/
Stream.prototype.latest = function() {
    var stream = new Stream();
    var newest = [];
    this.subscribe(function(s) {
        newest.push(s);
        s.subscribe(function(x) {
            var index = newest.indexOf(s);
            if (index >= 0) {
                newest = newest.slice(index);
                stream._push(x);
            }
        });
    });
    return stream;
}

Stream.prototype.unique = function(f) {
	this.seen = new Set();

	this._push = function(arg) {
		if (!this.seen.has(f(arg))) {
			for (var i = 0; i < this.callbacks.length; i++) {
				this.seen.add(f(arg));
				this.callbacks[i].apply(this, [arg]);
			}
		}
	};

	this._push_many = function(args) {
		// Need to save reference so we can use in our anonymous function
		for (var i = 0; i < args.length; i++) {
			this._push(args[i]);
		}
	};

    return this;
}

var FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

window.WIKICALLBACKS = {}

function WIKIPEDIAGET(s, cb) {
    $.ajax({
        url: "https://en.wikipedia.org/w/api.php",
        dataType: "jsonp",
        jsonp: "callback",
        data: {
            action: "opensearch",
            search: s,
            namespace: 0,
            limit: 10,
        },
        success: function(data) {cb(data[1])},
    })
}

$(function() {
    // PART 2 INTERACTIONS HERE
    Stream.timer(1000)
    .subscribe(function(d){$("#time").text(d.toString());});

    var clicks = 0;
    Stream.dom($("#button"), "click")
    .subscribe(function() {
        clicks += 1;
        $("#clicks").text(clicks);
    });

    Stream.dom($("#mousemove"), "mousemove")
    .throttle(500)
    .subscribe(function(e) {
        $("#mouseposition").text(`(${e.screenX}, ${e.screenY})`);
    });

    Stream.dom($("#wikipediasearch"), "input")
    .throttle(1000)
    .map(function(e) {
        var stream = new Stream();
        if (e.target.value == "") {
            $("#wikipediasuggestions").empty();
        } else {
            WIKIPEDIAGET(e.target.value, function(x) {
                $("#wikipediasuggestions").empty();
                stream._push(x)
            });
        }
        return stream;
    })
    .latest()
    .flatten()
    .subscribe(function(x) {
        $("#wikipediasuggestions").append($("<li></li>").text(x));
    });


	var fires = Stream.url(FIRE911URL).flatten().unique(function(x) {return x});
	fires.subscribe(function(j) {
		$("#fireevents").append($("<li></li>").text(j[3479077]));
	});

	var fireLocations = []
	fires.subscribe(function(j) {
		fireLocations.push(j);
	})

    // Get an additional list every 1 minute
    Stream.timer(60000).subscribe( function (x) {
		fires
	});

	Stream.dom($("#firesearch"), "input").subscribe(function(e) {
		let s = e.target.value;
		console.log("test");
		$("#fireevents").empty();
		if (s == "") {
			for (let l of fireLocations) {
				$("#fireevents").append($("<li></li>").text(l[3479077]));
			}
		} else {
			for (let l of fireLocations) {
				if (l[3479077].indexOf(s) == 0) {
					$("#fireevents").append($("<li></li>").text(l[3479077]));
				}
			}
		}
	})
});

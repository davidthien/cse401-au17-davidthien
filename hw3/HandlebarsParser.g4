parser grammar HandlebarsParser;

options { tokenVocab=HandlebarsLexer; }

document : element* EOF ;

element
    : rawElement
    | expressionElement
    | blockElement
    | commentElement
    ;

rawElement
    : (BRACE? TEXT)+
    ;

commentElement : START COMMENT END_COMMENT ;

expressionElement returns [String source] : START expression END;

expression returns [String source]
    : literal
    | lookup
    | parenExpression
    | helper
    ;

    //expression returns [String source]
    //    : literal {$source = $literal.source;}
    //    | lookup {$source = $lookup.source;}
    //    | parenExpression {$source = $parenExpression.source;}
    //    | helper {$source = $helper.source;}
    //    ;

lookup returns [String source]
    : ID
    ;

helper
    : a=lookup (expression)*
    ;

parenExpression returns [String source]
    : OPEN_PAREN expression CLOSE_PAREN
    ;

literal returns [String source] 
    : INTEGER
    | FLOAT
    | STRING
    ;

blockElement
    : START BLOCK helper END element+ START CLOSE_BLOCK ID END
    ; 

const ctx = {
	title: "Grimtooth's Traps",
	sections: [
		{
			title: "Room Traps",
			traps: [
				{name: "Infamous Wheel Trap"}, 
				{name: "Roman Amphitheatre Trap"},
				{name: "Illusions"}, 
				{name: "Slider Spike"}
			]
		}, 
		{
			title: "Corridor Traps",
			traps: [
				{name: "Hop, Skip, and a Jump"},
				{name: "The Gas Passage"},
				{name: "Sectioning Corridor"},
				{name: "Archer's Tunnel"}
			]
		}, 
		{
			title: "Door Traps",
			traps: [
				{name: "The Circular Doorway"},
				{name: "Giant's Razor"},
				{name: "The Guillotine Door Trap"}
			]
		}
	],
	author: "Grimtooth"
}

exports.ctx = ctx

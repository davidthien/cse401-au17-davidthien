function makeURI(ctx, course, year, quarter) {
   var quarterId = quarter.slice(0, 2).toLowerCase();
   return `https://${ctx.domain}/${course}/${year%100}${quarterId}`;
}

function concat(ctx, ...params) {
   return params.join("");
}

function skip(ctx, body) {
    return "";
}

const ctx = {
    title: "CSE 401: Domain-Specific Languages",
    domain: "https://www.cs.washington.edu/education/courses",
    year: "2017",
    quarter: "Autumn"
};

exports.helpers = [
    ["makeURI", makeURI],
    ["concat", concat]
];
exports.blockHelpers = [
    ["skip", skip]
];
exports.ctx = ctx;

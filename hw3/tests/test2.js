function append(ctx, start, end) {
	return start + end;
}

const ctx = {
	title: "Cool test",
	list: [
		{word: "this", append: " other"},
		{word: "is", append: " thing"},
		{word: "so", append: " is"},
		{word: "cool", append: " something"}
	],
};

exports.helpers = [
	["append", append]
];
exports.ctx = ctx;

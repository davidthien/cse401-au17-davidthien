const fs = require("fs");
const Compiler = require("./compiler").HandlebarsCompiler;
const compiler = new Compiler();

// register some sample helpers
compiler.registerExprHelper("makeURI", function (ctx, course, year, quarter) {
   var quarterId = quarter.slice(0, 2).toLowerCase();
   return `https://${ctx.domain}/${course}/${year%100}${quarterId}`;
});
compiler.registerExprHelper("concat", function (ctx, ...params) {
   return params.join("");
});
compiler.registerExprHelper("contains", function (ctx, str, substr) {
    return str.includes(substr);
});

// read input and run
let inputDoc = "";

process.stdin.setEncoding("utf8");

process.stdin.on("readable", () => {
    const chunk = process.stdin.read();
    if (chunk) {
        inputDoc += chunk;
    }
});

process.stdin.on("end", () => {
    if (process.argv.length > 2) {
        const data = fs.readFileSync(process.argv[2], {flag:'r', encoding:'utf-8'});
        const ctx = eval(data);
        const f = compiler.compile(inputDoc);
        console.log(f(ctx));
    } else {
        console.log(compiler.compile(inputDoc).toString());
    }
});

const antlr4 = require('antlr4');
const HandlebarsLexer = require('./gen/HandlebarsLexer').HandlebarsLexer;
const HandlebarsParser = require('./gen/HandlebarsParser').HandlebarsParser;
const HandlebarsParserListener = require('./gen/HandlebarsParserListener').HandlebarsParserListener;

function HandlebarsCompiler() {
    HandlebarsParserListener.call(this);
    this._inputVar = "__$ctx";
    this._outputVar = "__$result";
    this._helpers = { expr: {}, block: {} };
    this._usedHelpers = [];

    this.registerBlockHelper('each', function(ctx, body, list) {
        let result = "";
        for (let e of list) {
            result += body(e);
        }
        return result;
    });

    this.registerBlockHelper('if', function(ctx, body, expr) {
        if (expr) {
            return body(ctx);
        } else {
            return "";
        }
    });

    this.registerBlockHelper('with', function(ctx, body, field) {
        return body(ctx[field]);
    });

    return this;
}

HandlebarsCompiler.prototype = Object.create(HandlebarsParserListener.prototype);
HandlebarsCompiler.prototype.constructor = HandlebarsCompiler;

HandlebarsCompiler.mangle = function (id) {
    return "__$" + id;
};

HandlebarsCompiler.escape = function (string) {
    return ('' + string).replace(/["'\\\n\r\u2028\u2029]/g, function (c) {
        switch (c) {
            case '"':
            case "'":
            case '\\':
                return '\\' + c;
            case '\n':
                return '\\n';
            case '\r':
                return '\\r';
            case '\u2028':
                return '\\u2028';
            case '\u2029':
                return '\\u2029';
        }
    })
};

HandlebarsCompiler.prototype.registerExprHelper = function(name, helper) {
    this._helpers.expr[name] = helper;
};

HandlebarsCompiler.prototype.registerBlockHelper = function (name, helper) {
    this._helpers.block[name] = helper;
};


HandlebarsCompiler.prototype.pushScope = function () {
    this._bodyStack.push(`var ${this._outputVar} = "";\n`);
};
 
HandlebarsCompiler.prototype.popScope = function() {
    return this._bodyStack.pop() + `return ${this._outputVar};\n`;
};

HandlebarsCompiler.prototype.compile = function (template) {
    this._bodyStack = [];
    this.pushScope();
    this._usedHelpers = [];

    const chars = new antlr4.InputStream(template);
    const lexer = new HandlebarsLexer(chars);
    const tokens = new antlr4.CommonTokenStream(lexer);
    const parser = new HandlebarsParser(tokens);
    parser.buildParseTrees = true;
    const tree = parser.document();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

    var bodySource = this.popScope();
    var functionDeclarations = "";
    for (var i = 0, len = this._usedHelpers.length; i < len; i++) {
        var helperName = this._usedHelpers[i];
        var helperCode = this._helpers.expr[helperName];
        if (helperCode == undefined) {
            helperCode = this._helpers.block[helperName];
        }
        functionDeclarations += `var ${HandlebarsCompiler.mangle(helperName)} = ${helperCode.toString()};\n`;
    }
    bodySource = functionDeclarations + bodySource;
    return new Function(this._inputVar, bodySource);
};

HandlebarsCompiler.prototype.append = function (expr) {
    this._bodyStack.push(this._bodyStack.pop() + `${this._outputVar} += ${expr};\n`);
};

HandlebarsCompiler.prototype.exitRawElement = function (ctx) {
    this.append(`"${HandlebarsCompiler.escape(ctx.getText())}"`);
};

HandlebarsCompiler.prototype.exitLiteral = function (ctx) {
    ctx.source = ctx.getText();
};

HandlebarsCompiler.prototype.exitLookup = function (ctx) {
    ctx.source = `${this._inputVar}.${ctx.getText()}`    
};

HandlebarsCompiler.prototype.exitHelper = function (ctx) {
    var name = ctx.a.getText();
    this._usedHelpers.push(name);
    ctx.source = `${HandlebarsCompiler.mangle(name)}(${this._inputVar}, ${ctx.children.map(x=>x.source).slice(1).join(", ")})`;
};

HandlebarsCompiler.prototype.exitExpression = function (ctx) {
    ctx.source = ctx.children[0].source;
};

HandlebarsCompiler.prototype.exitParenExpression = function (ctx) {
    ctx.source = ctx.children[1].source;
};

HandlebarsCompiler.prototype.exitExpressionElement = function (ctx) {
    this.append(ctx.children[1].source);
};

HandlebarsCompiler.prototype.enterBlockElement = function (ctx) {
    this.pushScope();
};

HandlebarsCompiler.prototype.exitBlockElement = function (ctx) {
    var blockName = ctx.children[2].a.getText();
    var endBlockName = ctx.children[ctx.children.length - 2].getText();
    if (blockName != endBlockName) {
        throw ("Block start '" + blockName + "' does not match the block end '" + endBlockName + "'.");
    }
    var blockBody = new Function(this._inputVar, this.popScope());
    var args = ctx.children[2].children.map(x => x.source).slice(1);
    args.unshift(blockBody.toString());
    var helperCall = `${HandlebarsCompiler.mangle(blockName)}(${this._inputVar}, ${args.join(", ")})`;
    this.append(helperCall);
};

exports.HandlebarsCompiler = HandlebarsCompiler;

const fs = require("fs");
const Compiler = require("./compiler").HandlebarsCompiler;

function setupCompiler(ctxName) {
    const ctx = require("./" + ctxName);
    const compiler = new Compiler();
    if (ctx.helpers) {
        for (const [name, f] of ctx.helpers) {
            compiler.registerExprHelper(name, f);
        }
    }
    let compileCtx = {};
    if (ctx.ctx) {
        compileCtx = ctx.ctx;
    }
    return [compiler, compileCtx];
}

function compileFile(fileName, compiler, ctx) {
    const data = fs.readFileSync(fileName, {flag:'r', encoding:'utf-8'});
    const f = compiler.compile(data);
    return f(ctx);
}

function compareOutput(refName, output) {
    const refData = fs.readFileSync(refName, {flag:'r', encoding:'utf-8'});
    return output === refData;
}

function runTest(ctxName, infile, outfile) {
    const [compiler, ctx] = setupCompiler(ctxName);
    const output = compileFile(infile, compiler, ctx);
    return compareOutput(outfile, output);
}

function simpleTest(pref) {
    return runTest(pref + '.js', pref + '.input', pref + '.output');
}

exports.setupCompiler = setupCompiler;
exports.compileFile = compileFile;
exports.compareOutput = compareOutput;
exports.runTest = runTest;
exports.simpleTest = simpleTest;

if (!module.parent) {
    if (process.argv.length == 3) {
        const success = simpleTest(process.argv[2]);
        if (success) {
            console.log("Test " + process.argv[2] + " passed.\n");
        } else {
            console.log("Test " + process.argv[2] + " failed.\n");
        }
    } else if (process.argv.length == 5) {
        const [compiler, ctx] = setupCompiler(process.argv[2]);
        console.log("loaded compiler and context from: " + process.argv[2] + "\n");
        console.log(compiler);
        console.log("");
        
        console.log("ctx=");
        console.log(ctx);
        console.log("\n");

        const inputData = fs.readFileSync(process.argv[3], {flag:'r', encoding:'utf-8'});
        console.log("read input file: " + process.argv[3]);
        console.log(inputData);
        console.log("");

        const f = compiler.compile(inputData);
        console.log("file compiled to function:");
        console.log(f.toString());
        console.log("");

        const output = f(ctx);
        console.log("applied to context:");
        console.log(output);
        console.log("");

        const outputData = fs.readFileSync(process.argv[4], {flag:'r', encoding:'utf-8'});
        console.log("read (expected) output file: " + process.argv[4]);
        console.log(outputData);
        console.log("");

        if (output === outputData) {
            console.log("test: passing");
        } else {
            console.log("test: failing");
        }
    } else {
        console.log("usage: node testdriver.js TESTNAME\n"
                    + "     | node testdriver.js CTX.js INPUT.input EXPECTED.output\n");
    }
}
